package com.gabrielCode.rest;
import org.springframework.web.bind.annotation.RestController;

import com.gabrielCode.model.Persona;
import com.gabrielCode.repo.IPersonaRepo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
@RestController
@RequestMapping("/personas")
public class RestDemoController {
@Autowired
private IPersonaRepo repo;
	
@GetMapping
public List<Persona> listar() {
	return repo.findAll();
}
@PostMapping 
public void insertar(@RequestBody Persona per) {
	repo.save(per);
	
}

}
