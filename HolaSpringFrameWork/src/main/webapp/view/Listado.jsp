<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${titulo}</title>
</head>
<body>
<ul>
<c:forEach items="${lista}" var="lista">
<li>
<c:out value="${lista.getNombre()}" />
<c:out value="${lista.getApellido()}" />
<c:out value="${lista.getEstudios()}" />
<c:out value="${lista.getLinkArepositorio()}" />
<br></br>
</li>
</c:forEach>
</ul>
</body>
</html>