package es.edu.alter.practica0.model;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		this("tijera",TIJERA);
	}
	public Tijera (String pNom,int pNum) {
		super(pNom,pNum);
			}
	public boolean isMe(int pNum) {
		return pNum==TIJERA;		
	}
	public int comparar (PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PIEDRA:
		case SPOCK:	
			resul=-1;
			this.descripcionResultado="tijera pierde con "+ pPiedPapelTijera.getNombre();			
			break;
		case PAPEL:
		case LAGARTO:	
			resul=1;
			this.descripcionResultado="tijera gana a "+ pPiedPapelTijera.getNombre();			
			break;
			default:
				resul=0;
				this.descripcionResultado="tijera empata con "+ pPiedPapelTijera.getNombre();
				break;
				
		}
		return resul;
	} 
}
