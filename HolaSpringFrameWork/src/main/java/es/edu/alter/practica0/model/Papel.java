package es.edu.alter.practica0.model;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		this("papel",PAPEL);
	}
	public Papel (String pNom,int pNum) {
		super(pNom,pNum);
			}
	public boolean isMe(int pNum) {
		return pNum==PAPEL;		
	}
	public int comparar (PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PIEDRA:
		case SPOCK:
			resul=1;
			this.descripcionResultado="papel gana a "+ pPiedPapelTijera.getNombre();
			break;
		case TIJERA:
		case LAGARTO:	
			resul=-1;
			this.descripcionResultado="papel pierde con "+ pPiedPapelTijera.getNombre();
			break;
			default:
				resul=0;
				this.descripcionResultado="papel empata con "+ pPiedPapelTijera.getNombre();
				break;
				
		}
		return resul;
	} 
}
