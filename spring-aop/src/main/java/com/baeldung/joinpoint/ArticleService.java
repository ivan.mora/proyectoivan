package com.baeldung.joinpoint;



import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


@Service
public class ArticleService {

    public List<String> getArticleList() {
        return Arrays.asList(
          "Article 1",
          "Article 2"
        );
    }

    public List<String> getArticleList(String startsWithFilter) {
        if (StringUtils.isBlank(startsWithFilter)) {
            throw new IllegalArgumentException("startsWithFilter can't be blank");
        }

        return getArticleList()
          .stream()
          .filter(a -> a.startsWith(startsWithFilter))
          .collect(Collectors.toList());
    }

}
