package es.edu.alter.practica0.modelo;

import java.util.Objects;

import es.edu.alter.practica0.model.PiedraPapelTijeraFactory;

public class Jugador {

	//atributos
		private int 	codigo				;
		private String 	nombre				;
		private String 	nickName			;
		private PiedraPapelTijeraFactory 	jugadaElegida	;

		public Jugador() {
			
			
		}
		
	public Jugador(int pCodigo, String pNom,String pNick, PiedraPapelTijeraFactory pJugada) {
		codigo=pCodigo;
		nombre=pNom;
		nickName=pNick;
		jugadaElegida=pJugada;
		// TODO Auto-generated constructor stub
	}

					public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public PiedraPapelTijeraFactory getJugadaElegida() {
		return jugadaElegida;
	}

	public void setJugadaElegida(PiedraPapelTijeraFactory jugadaElegida) {
		this.jugadaElegida = jugadaElegida;
	}

					@Override
					public String toString() {
						return "Jugador [codigo=" + codigo + ", nombre=" + nombre + ", nickname=" + nickName
								+ ", jugadaElegida=" + jugadaElegida + "]";
					}

					@Override
					public int hashCode() {
						return Objects.hash(codigo, nickName, nombre);
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						Jugador other = (Jugador) obj;
						return codigo == other.codigo && Objects.equals(nickName, other.nickName)
								&& Objects.equals(nombre, other.nombre);
					}

					
					
					
				
}
