package es.edu.alter.practica0.modelo;

public class Lagarto extends PiedraPapelTijeraFactory {

	public Lagarto() {
		this("lagarto",LAGARTO);
	}
	public Lagarto (String pNom,int pNum) {
		super(pNom,pNum);
			}
	public boolean isMe(int pNum) {
		return pNum==LAGARTO;		
	}
	public int comparar (PiedraPapelTijeraFactory pPiedPapelTijera) {
		
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case TIJERA:
		case PIEDRA:	
			resul=-1;
			this.descripcionResultado="lagarto pierde con "+ pPiedPapelTijera.getNombre();			
			break;
		case PAPEL:
		case SPOCK:	
			resul=1;
			this.descripcionResultado="lagarto gana a "+ pPiedPapelTijera.getNombre();			
			break;
			default:
				resul=0;
				this.descripcionResultado="lagarto empata con "+ pPiedPapelTijera.getNombre();
				break;
				
		}
		return resul;
	}
		
}