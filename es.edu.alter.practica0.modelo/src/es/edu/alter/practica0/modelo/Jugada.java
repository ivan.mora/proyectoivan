package es.edu.alter.practica0.modelo;

import java.util.Date;
import java.util.Objects;

public class Jugada {

	//atributos
			private int 	codigo				;
			private Date 	fechaHora			;
			private Jugador	jugador1,jugador2	;
			
			public Jugada() {
				
			}
			
		public Jugada(int pCodigo, Date pFechaHora,Jugador pJugador1, Jugador pJugador2 ) {
			codigo=pCodigo;
			fechaHora=pFechaHora;
			jugador1=pJugador1;
			jugador2=pJugador2;
			
			// TODO Auto-generated constructor stub
		}

	
		
	
						public int getCodigo() {
			return codigo;
		}

		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}

		public Date getFechaHora() {
			return fechaHora;
		}

		public void setFechaHora(Date fechaHora) {
			this.fechaHora = fechaHora;
		}

		public Jugador getJugador1() {
			return jugador1;
		}

		public void setJugador1(Jugador jugador1) {
			this.jugador1 = jugador1;
		}

		public Jugador getJugador2() {
			return jugador2;
		}

		public void setJugador2(Jugador jugador2) {
			this.jugador2 = jugador2;
		}

						@Override
						public String toString() {
							return "Jugada [codigo=" + codigo + ", fechaHora=" + fechaHora + ", jugador1=" + jugador1
									+ ", jugador2=" + jugador2 + "]";
						}

						

											
				

						

						@Override
						public int hashCode() {
							return Objects.hash(codigo, fechaHora);
						}

						@Override
						public boolean equals(Object obj) {
							if (this == obj)
								return true;
							if (obj == null)
								return false;
							if (getClass() != obj.getClass())
								return false;
							Jugada other = (Jugada) obj;
							return codigo == other.codigo && Objects.equals(fechaHora, other.fechaHora);
						}

						public String getDescripcionREsultado() {
							jugador1.getJugadaElegida().comparar(jugador2.getJugadaElegida());
							return jugador1.getJugadaElegida().getDescripcionREsultado();
							
							
						}
					

					
}
