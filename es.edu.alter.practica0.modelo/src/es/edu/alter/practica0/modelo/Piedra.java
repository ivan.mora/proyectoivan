package es.edu.alter.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra () {
		this("piedra",PIEDRA);
	}
	public Piedra (String pNom,int pNum) {
		super(pNom,pNum);
			}
	public boolean isMe(int pNum) {
		return pNum==PIEDRA;		
	}
	public int comparar (PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PAPEL:
		case SPOCK:		
			resul=-1;
			this.descripcionResultado="piedra pierde con "+ pPiedPapelTijera.getNombre();
			break;
		case TIJERA:
		case LAGARTO:	
			resul=1;
			this.descripcionResultado="piedra gana a "+ pPiedPapelTijera.getNombre();
			break;
			default:
				resul=0;
				this.descripcionResultado="piedra empata con "+ pPiedPapelTijera.getNombre();
				break;
				
		}
		return resul;
	} 
}
